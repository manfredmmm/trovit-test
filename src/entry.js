// Chartjs
import Chart from 'chart.js';
import Vue from '../node_modules/vue/dist/vue';
// Data
import data from './js/data.json';
// Stylesheets
import './styles/application.scss';

document.addEventListener('DOMContentLoaded', () => {
  // ------------------------------------------------------
  // Function to get the desired data
  // ------------------------------------------------------
  const getData = (key) => {
    const values = [];
    for (let i = 1; i <= Object.keys(data).length; i += 1) {
      switch (key) {
        case 'month':
          values.push(data[i].month);
          break;
        case 'price_m2':
          values.push(data[i].price_m2);
          break;
        default:
          break;
      }
    }
    return values;
  };

  const _myTable = new Vue({
    el: '#myTable',
    data: {
      rows: Object.keys(data).length,
      data
    }
  });

  // Creating data arrays for Line chartjs
  const months = getData('month');
  const prices = getData('price_m2');

  // ------------------------------------------------------
  // Line chart
  // ------------------------------------------------------
  const chartId = document.getElementById('myChart');
  const _myChart = new Chart(chartId, {
    type: 'line',
    data: {
      labels: months,
      datasets: [{
        label: '€ / m2 Barcelona',
        fill: true,
        lineTension: 0,
        backgroundColor: 'rgba(94,180,239,0.4)',
        borderColor: 'rgba(94,180,239,1)',
        borderWidth: 1,
        pointBorderColor: 'rgba(94,180,239,1)',
        pointBackgroundColor: 'transparent',
        pointBorderWidth: 1,
        pointHoverRadius: 3,
        pointHoverBackgroundColor: 'rgba(94,180,239,0.4)',
        pointHoverBorderColor: 'rgba(94,180,239,1)',
        pointHoverBorderWidth: 1,
        pointRadius: 3,
        pointHitRadius: 3,
        data: prices,
        pointStyle: 'circle'
      }]
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      }
    }
  });
});
