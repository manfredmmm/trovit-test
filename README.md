# Trovit test #
## Tools used: ##
*   Webpack 2 (grunt alternative)
*   Pug templates
*   Babel ES6
*   Yarn (npm alternative)
*   SASS
*   ChartJS
*   Bootstrap
*   Vuejs

## Usage: ##
* `yarn` (to install dependencies)
* `yarn start` (to start the project in local environment)
* `webpack` (to generate /dist files)
* `webpack -p` (to generate /dist minified files)

## Live demo at: ##
[http://trovit-test.surge.sh](http://trovit-test.surge.sh)